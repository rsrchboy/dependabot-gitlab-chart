.mr: &mr
  if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
.master: &master
  if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"'
.release: &release
  if: '$CI_COMMIT_TAG =~ /^v[0-9.]+$/ && $CI_PIPELINE_SOURCE == "push"'

.chart_changes: &chart_changes
  - charts/**/*
  - .helmignore
  - .gitlab/ci/**/*
  - .gitlab-ci.yml

.main: &main
  - <<: *mr
    changes: *chart_changes
  - <<: *master
    changes: *chart_changes
  - *release

.install:
  stage: test
  services:
    - docker:20.10.12-dind
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: /certs
    DOCKER_CERT_PATH: /certs/client
    DOCKER_TLS_VERIFY: 1
    NAMESPACE: dependabot
    RELEASE_NAME: dependabot
  parallel:
    matrix:
      - VALUES: [default, secrets, ingress]
  before_script:
    - .gitlab/ci/script/install-kind.sh
    - .gitlab/ci/script/setup-cluster.sh
  script:
    - .gitlab/ci/script/install-app.sh
  after_script:
    - .gitlab/ci/script/log-install.sh
  rules: *main

stages:
  - build
  - static analysis
  - test
  - release

variables:
  CHART_DIR: charts/dependabot-gitlab

image: registry.gitlab.com/dependabot-gitlab/dependabot/ci:latest

build-helm-chart:
  stage: build
  script:
    - .gitlab/ci/script/build-chart.sh
  artifacts:
    paths:
      - public/index.yaml
      - dependabot-gitlab-*.tgz
  rules:
    - *mr
    - *master
    - *release

lint-chart:
  stage: static analysis
  needs: []
  script:
    - .gitlab/ci/script/lint.sh
  rules: *main

lint-docs:
  stage: static analysis
  needs: []
  script:
    - .gitlab/ci/script/helm-docs.sh
  rules: *main

kubeval:
  stage: static analysis
  needs: []
  script:
    - .gitlab/ci/script/kubeval.sh
  rules: *main

install:
  extends: .install

upgrade:
  extends: .install

pages:
  stage: release
  needs:
    - build-helm-chart
    - lint-chart
    - lint-docs
    - kubeval
    - install
  before_script:
    - cp artifacthub-repo.yml public/
  script:
    - .gitlab/ci/script/publish-chart.sh
  artifacts:
    paths:
      - public
  rules:
    - *release

changelog:
  stage: release
  needs:
    - pages
  variables:
    RELEASE_NOTES_FILE: release_notes.md
  script:
    - .gitlab/ci/script/changelog.sh
  release:
    tag_name: $CI_COMMIT_TAG
    description: $RELEASE_NOTES_FILE
  rules:
    - *release
