#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log2 "Update chart dependencies"
helm repo add bitnami https://charts.bitnami.com/bitnami
helm dependency build $CHART_DIR --skip-refresh

log2 "Validate chart template files"
for yaml in .gitlab/ci/kube/values/*.yaml; do
  log2 "Validating $CHART_DIR with $yaml" "-"

  helm template $CHART_DIR -f $yaml | kubeval \
    --strict \
    --schema-location https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master \
    --additional-schema-locations https://raw.githubusercontent.com/joshuaspence/kubernetes-json-schema/master
done
