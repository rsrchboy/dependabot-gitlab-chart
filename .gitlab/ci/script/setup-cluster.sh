#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log2 "Setup cluster"

log "Create '$NAMESPACE' namespace"
kubectl create namespace "$NAMESPACE"

log "Create serviceMonitor CRD"
kubectl create -f https://raw.githubusercontent.com/prometheus-operator/kube-prometheus/main/manifests/setup/0servicemonitorCustomResourceDefinition.yaml

log "Install gitlab mock"
echo "** Adding repo **"
helm repo add andrcuns https://andrcuns.github.io/charts

echo "** Installing smocker chart **"
helm install gitlab andrcuns/smocker \
  -n gitlab \
  -f .gitlab/ci/config/mock-definitions.yaml \
  --create-namespace \
  --timeout 2m \
  --atomic

if [[ "$VALUES" == "secrets" ]]; then
  log "Create custom secrets for credentials"
  kubectl create -f .gitlab/ci/kube/secrets.yaml -n "$NAMESPACE"
elif [[ "$VALUES" == "ingress" ]]; then
  log "Setup ingress"
  kubectl apply -f https://github.com/datawire/ambassador-operator/releases/latest/download/ambassador-operator-crds.yaml
  kubectl apply -n ambassador -f https://github.com/datawire/ambassador-operator/releases/latest/download/ambassador-operator-kind.yaml

  log "Wait for ingress to become available"
  kubectl wait --timeout=180s -n ambassador --for=condition=deployed ambassadorinstallations/ambassador
fi
